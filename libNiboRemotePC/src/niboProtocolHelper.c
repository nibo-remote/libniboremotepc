/**
 @file		niboProtocolHelper.c
 @brief		Defines macros, which let you easily execute commands known from nibolib.
 @author	Tobias Ilte
 @version	1.0
 @date		01.07.2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <math.h> // ATTENTION: math.h has to be linked in compiler! "gcc -lm"
#include <sys/ioctl.h>

#include "niboProtocolParser.h"
#include "niboProtocolHelper.h"


enum {
	LEDS_OFF = 0, LEDS_GREEN = 1, LEDS_RED = 2, LEDS_ORANGE = 3
};

void leds_set_status(uint8_t color, uint8_t led) {
	uint8_t ledBinary = pow(2, led);
	switch (color) {
		case LEDS_OFF:
			registerFlags[NSPREG_LEDSR] |= NSP_FLAG_SET;
			registerFlags[NSPREG_LEDSG] |= NSP_FLAG_SET;
			registerValues[NSPREG_LEDSR] &= ~ledBinary;
			registerValues[NSPREG_LEDSG] &= ~ledBinary;
			break;
		case LEDS_GREEN:
			registerFlags[NSPREG_LEDSG] |= NSP_FLAG_SET;
			registerValues[NSPREG_LEDSG] |= ledBinary;
			break;
		case LEDS_RED:
			registerFlags[NSPREG_LEDSR] |= NSP_FLAG_SET;
			registerValues[NSPREG_LEDSR] |= ledBinary;
			break;
		case LEDS_ORANGE:
			registerFlags[NSPREG_LEDSR] |= NSP_FLAG_SET;
			registerFlags[NSPREG_LEDSG] |= NSP_FLAG_SET;
			registerValues[NSPREG_LEDSG] |= ledBinary;
			registerValues[NSPREG_LEDSR] |= ledBinary;
			break;
		default:
			break;
	}
}

void leds_set_headlights(int light) {
	registerFlags[NSPREG_LEDSWPWM] |= NSP_FLAG_SET;
	registerValues[NSPREG_LEDSWPWM] = light;
}

void copro_stopImmediate() {
	registerFlags[NSPREG_MOTMODE] |= NSP_FLAG_SET;
	registerValues[NSPREG_MOTMODE] = 0;
}

void copro_stop() {
	registerFlags[NSPREG_MOTMODE] |= NSP_FLAG_SET;
	registerValues[NSPREG_MOTMODE] = 1;
}

void copro_setPWM(int16_t left, int16_t right) {
	registerFlags[NSPREG_MOTMODE] |= NSP_FLAG_SET;
	registerFlags[NSPREG_MOTPWML] |= NSP_FLAG_SET;
	registerFlags[NSPREG_MOTPWMR] |= NSP_FLAG_SET;
	registerValues[NSPREG_MOTMODE] = 2;
	registerValues[NSPREG_MOTPWML] = left;
	registerValues[NSPREG_MOTPWMR] = right;
}

void copro_setSpeed(int16_t left, int16_t right) {
	registerFlags[NSPREG_MOTMODE] |= NSP_FLAG_SET;
	registerFlags[NSPREG_MOTPIDL] |= NSP_FLAG_SET;
	registerFlags[NSPREG_MOTPIDR] |= NSP_FLAG_SET;
	registerValues[NSPREG_MOTMODE] = 3;
	registerValues[NSPREG_MOTPIDL] = left;
	registerValues[NSPREG_MOTPIDR] = right;
}
/*
void get_copro_current_l(uint8_t requestMode){
	registerFlags[11] |= requestMode;
}

void get_copro_current_r(uint8_t requestMode){
	registerFlags[12] |= requestMode;
}

void get_copro_ticks_l(uint8_t requestMode){
	registerFlags[13] |= requestMode;
}

void get_copro_ticks_r(uint8_t requestMode){
	registerFlags[14] |= requestMode;
}

void get_floor_relative_floor_left(uint8_t requestMode){
	registerFlags[15] |= requestMode;
}

void get_floor_relative_floor_right(uint8_t requestMode){
	registerFlags[16] |= requestMode;
}

void get_floor_relative_line_left(uint8_t requestMode){
	registerFlags[17] |= requestMode;
}

void get_floor_relative_line_left(uint8_t requestMode){
	registerFlags[18] |= requestMode;
}

void get_copro_distance_left(uint8_t requestMode){
	registerFlags[19] |= requestMode;
}

void get_copro_distance_front_left(uint8_t requestMode){
	registerFlags[20] |= requestMode;
}

void get_copro_distance_front(uint8_t requestMode){
	registerFlags[21] |= requestMode;
}

void get_copro_distance_front_right(uint8_t requestMode){
	registerFlags[22] |= requestMode;
}

void get_copro_distance_right(uint8_t requestMode){
	registerFlags[23] |= requestMode;
}

void get_nds3_get_dist(uint8_t requestMode){
	registerFlags[24] |= requestMode;
}
*/
