/**
 @file		connectionService.c
 @brief		All platform-specific functions regarding the transfer of data via serial port.
 @author	Tobias Ilte
 @version	1.0
 @date		01.07.2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include <sys/ioctl.h>
#include <fcntl.h>		// file-descriptor functions
#include <inttypes.h>	// more c99 int-types
#include <unistd.h> 	// unix-standard calls
#include <termios.h> 	// terminal i/o functions
#include <time.h>   	// time calls

#include "connectionService.h"
#include "niboProtocolParser.h"


#define FILENAME "/dev/ttyUSB0"

static char buffer[512]; //buffer for receiving and transmitting data
static int pos = 0; //points to last value in buffer[]
static int len;

int open_port(void) {
	int fd; // file description for the serial port

	//open port with read/write
	// cutecom: nur: NDELAY | O_RDWR
	fd = open(FILENAME, O_RDWR | O_NOCTTY | O_NONBLOCK);
	//fd = open("/dev/rfcomm0", O_RDWR | O_NOCTTY | O_NONBLOCK);

	if (fd == -1) { // if open is unsucessful.
		perror("open_port: Unable to open Port");
		printf("Unable to open Port!\n");
	}
	tcflush(fd, TCIOFLUSH);

	int n = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, n & ~O_NDELAY);

	return (fd);
}

int configure_port(int fd) {     // configure the port
	struct termios port_settings;      // structure to store the port settings in

	if (!isatty(fd)) {
		printf("No terminal device!\n");
	}
	if (tcgetattr(fd, &port_settings) < 0) {
		printf("Can't get port settings!\n");
		perror("stdin");
	}

	cfsetispeed(&port_settings, (speed_t) B9600);    // set baud rates
	cfsetospeed(&port_settings, (speed_t) B9600);

	port_settings.c_cflag &= ~PARENB;    // set no parity-bit
	port_settings.c_cflag &= ~CSTOPB;	// 1 stop bit
	port_settings.c_cflag &= ~CSIZE;	// 8 data bits
	port_settings.c_cflag |= CS8;		// 8 data bits

	port_settings.c_cflag |= CLOCAL | CREAD;
	port_settings.c_cflag &= ~CRTSCTS;
	port_settings.c_iflag = IGNBRK;
	port_settings.c_iflag &= ~(IXON | IXOFF | IXANY);

	port_settings.c_lflag = 0;
	port_settings.c_oflag = 0;

	port_settings.c_cc[VTIME] = 1;
	port_settings.c_cc[VMIN] = 60;

	tcsetattr(fd, TCSANOW, &port_settings);    // apply the settings to the port
	int mcs = 0;
	ioctl(fd, TIOCMGET, &mcs);
	mcs |= TIOCM_RTS;
	ioctl(fd, TIOCMSET, &mcs);

	return (fd);
}

void writeData(int leng) {
	pos = 0;
	while (pos < leng) {
		write(glob_fd, &buffer[pos], 1);
		usleep(1000);
		printf("Writing data:%d\n", buffer[pos]);
		pos++;
	}
	printf("Data written!\n");
	pos = 0;
}

void submitData() {
	len = buildRequest(buffer, sizeof(buffer));
	writeData(len);
	readData();
}

void readData() {
	uint8_t bytesRead = 0;
	bytesRead = read(glob_fd, buffer, 512);
	parseAnswer(buffer, bytesRead);
}
