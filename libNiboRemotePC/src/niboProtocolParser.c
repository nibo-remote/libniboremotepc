/**
 @file		niboProtocolParser.c
 @brief		Parses all requests and replies and stores data into an array.
 @author	Tobias Ilte
 @version	1.0
 @date		01.07.2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "niboProtocolParser.h"

uint16_t registerValues[REGISTER_SIZE];
uint8_t registerFlags[REGISTER_SIZE];

static uint8_t error;
static char * data;
static uint16_t length;



static void parseSpace() {
	while (length) {
		char c = *data;
		if ((c == ' ') || (c == '\t') || (c == ',')) {
			data++;
			length--;
		} else {
			return;
		}
	}
}

static uint16_t parseHex() {
	uint16_t result = 0;
	while (length) {
		char c = *data;
		if ((c >= '0') && (c <= '9')) {
			data++;
			length--;
			result = result * 16 + (c - '0');
		} else if ((c >= 'a') && (c <= 'f')) {
			data++;
			length--;
			result = result * 16 + (c - ('a' - 10));
		} else {
			return result;
		}
	}
	return result;
}

static uint16_t parseInt() {
	uint16_t result = 0;
	uint8_t neg = 0;
	if (length) {
		if (*data == '-') {
			length--;
			data++;
			neg = 1;
		} else if (*data == '+') {
			length--;
			data++;
		}
	}
	result = parseHex();
	return (neg) ? (-result) : (result);
}

static void parseSet() {
	uint8_t id;
	uint16_t value;
	parseSpace();
	id = parseInt();
	parseSpace();
	value = parseInt();
	if (id >= REGISTER_SIZE) {
		error = 1;
		printf("\nError at writing into register.[%d]:%d\n", id, value);
	} else {
		printf("Writing data into register.[%d]:%d\n", id, value);
		registerValues[id] = value;
	}
}

void parseAnswer(char * rxdata, uint16_t rxlen) {
	data = rxdata;
	length = rxlen;

	while (length) {
		char c = *data;
		data++;
		length--;
		switch (c) {
			case 0:
				error = 1;
				break;
			case ' ':
			case '\t':
				continue;
			case '*':
				break;
			case '!':
				parseSet();
				break;
			default:
				printf("\nerror\n");
				error = 1;
				break;
		}
		if (error) {
			error = 0;
			break;
		}
	}
}

static void outputHex(uint16_t value) {
	data += sprintf(data, "%x", value);
}

uint16_t buildRequest(char * txdata, uint16_t txlenmax) {
	data = txdata;
	printf("data:%ld\n", (long) data);
	data[--txlenmax] = 0;

	*data++ = '$';

	for (uint8_t i = 0; i < REGISTER_SIZE; i++) {
		switch (registerFlags[i]) {
			case NSP_FLAG_GET:
				registerFlags[i] &= ~NSP_FLAG_GET;
				*data++ = '?';
				outputHex(i);
				break;
			case NSP_FLAG_UNREPORT:
				registerFlags[i] &= ~NSP_FLAG_UNREPORT;
				*data++ = '~';
				outputHex(i);
				break;
			case NSP_FLAG_REPORT:
				registerFlags[i] &= ~NSP_FLAG_REPORT;
				*data++ = '#';
				outputHex(i);
				break;
			case NSP_FLAG_SET:
				registerFlags[i] &= ~NSP_FLAG_SET;
				*data++ = '!';
				outputHex(i);
				*data++ = ',';
				printf("\nregisterValue:%d",registerValues[i]);
				outputHex(registerValues[i]);
				break;
			default:
				break;
		}
	}

	printf("data:%ld\n", (long) data);
	*data++ = '\n';
	printf("data:%ld\n", (long) data);
	printf("data-txdata:%ld\n", data - txdata);
	return (data - txdata);
}
