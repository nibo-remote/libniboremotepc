/**
 @file		niboProtocolHelper.h
 @brief		Defines macros, which let you easily execute commands known from nibolib.
 @author	Tobias Ilte
 @version	1.0
 @date		01.07.2013
 */

#ifndef NIBOPROTOCOLHELPER_H_
#define NIBOPROTOCOLHELPER_H_


enum{
	/* System */
	NSPREG_BOTID = 0,
	NSPREG_VERSION = 1,
	NSPREG_VSUPPLY = 2,
	/* LEDs */
	NSPREG_LEDSR = 3,
	NSPREG_LEDSG = 4,
	NSPREG_LEDSWPWM = 5,
	/* Motor */
	NSPREG_MOTMODE = 6,
	NSPREG_MOTPWML = 7,
	NSPREG_MOTPWMR = 8,
	NSPREG_MOTPIDL = 9,
	NSPREG_MOTPIDR = 10,
	NSPREG_MOTCURL = 11,
	NSPREG_MOTCURR = 12,
	/* Odometry */
	NSPREG_ODOL = 13,
	NSPREG_ODOR = 14,
	/* Floor/Line */
	NSPREG_FLOORL = 15,
	NSPREG_FLOORR = 16,
	NSPREG_LINEL = 17,
	NSPREG_LINER = 18,
	/* Distance */
	NSPREG_DIST_L = 19,
	NSPREG_DIST_FL = 20,
	NSPREG_DIST_F = 21,
	NSPREG_DIST_FR = 22,
	NSPREG_DIST_R = 23,
	NSPREG_DIST_NDS3 = 24
};



void leds_set_status(uint8_t color, uint8_t led);

void leds_set_headlights(int light);

void copro_stopImmediate();
void copro_stop();

void copro_setPWM(int16_t left, int16_t right);

void copro_setSpeed(int16_t left, int16_t right);

#endif /* NIBOPROTOCOLHELPER_H_ */
