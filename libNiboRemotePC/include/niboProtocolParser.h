/**
 @file		niboProtocolParser.h
 @brief		Parses all requests and replies and stores data into an array.
 @author	Tobias Ilte
 @version	1.0
 @date		01.07.2013
 */


#ifndef NIBOPROTOCOLPARSER_H_
#define NIBOPROTOCOLPARSER_H_

#include <stdint.h>

/// register flag, which indicates, that the value of the associated element are supposed to be sent
#define NSP_FLAG_SET		1

/// register flag, which indicates a request for that element
#define NSP_FLAG_GET		2

/// register flag, which indicates a request for automatically obtain the value of the associated element at every reply of the robot
#define NSP_FLAG_REPORT		4

/// register flag, which ends the report for this element
#define NSP_FLAG_UNREPORT	8

/// size of registerValue and registerFlags
#define REGISTER_SIZE 32


/**
	 * The array "registerValues" contains the sensor data of the robot.
	 * Every element of the array represents one sensor/component of the robot.
	 * The numbering is compatible with the Nibo Serial Protocol.
	 * E.g.: registerValues[13] represents the ticks/s of the left motor.
*/
extern uint16_t registerValues[REGISTER_SIZE];

/**
	 * The array "registerFlags" contains flags, which indicate how data is requested.
	 * It has the same numbering as registerValues. So f.ex. when you want to set the speed
	 * of the left motor to 10 ticks/s, you simply call:
	 * {@code registerFlags[13] |= NSP_FLAG_SET;}
	 * {@code registerValues[13] = 10;}
	 * Finally submit the request: {@code submitData();}
*/
extern uint8_t registerFlags[REGISTER_SIZE];

/// parses the answer-string and saves data into array
void parseAnswer(char * rxdata, uint16_t rxlen);

/// Builds String from flags in the registerFlags-array.
uint16_t buildRequest(char * txdata, uint16_t txlenmax);


#endif /* NNIBOPROTOCOLPARSER_H_ */
