/**
 @file		connectionService.h
 @brief		All platform-specific functions regarding the transfer of data via serial port.
 @author	Tobias Ilte
 @version	1.0
 @date		03.12.2012
*/

#ifndef CONNECTIONSERVICE_H_
#define CONNECTIONSERVICE_H_
#include <inttypes.h>


/**
 * open the port of the connected serial-device
 * @return the file description of the port
 */
int open_port(void);

/**
 * Configures the port. (Start-Bit, Baud-rate, parity-bit...)
 * @param fd port-variable
 * @return the file description of the port
 */
int configure_port(int fd);

/**
 * Receives data via XBEE from the nibo-robot.
 */
void readData();

/// builds request, sends data, and reads answer
void submitData();

/// send data via serial port
void writeData();


///file description of the uses serial port
int glob_fd;


#endif /* CONNECTIONSERVICE_H_ */
